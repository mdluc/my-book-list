## Dependencies

1. Node
2. Postgres

---

## Install

1. Clone project
2. ```npm install``` (into root folder)
3. ```npm install``` (into client folder)
4. install globally nodemon and sequelize-cli
5. config db for server
6. migrate db

---

## Run Project

Into root folder, run in terminal:

```npm run dev```

Visit: ```localhost:8080```