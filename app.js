const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cors = require('cors');

const booksRoutes = require('./server/routes/books');
const usersRoutes = require('./server/routes/users');
const reviewsRoutes = require('./server/routes/reviews');
const profileRoutes = require('./server/routes/profiles');

//logging every action in console
app.use(morgan('dev'));

//json parser from requests (Body Parser config)
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//handling CORS error
app.use(cors());

app.use((req,res,next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE');
        return res.status(200).json({});
    }

    next();
});

//these are our api routs
app.use('/books', booksRoutes);
app.use('/users', usersRoutes);
app.use('/reviews', reviewsRoutes);
app.use('/profile', profileRoutes)
app.use(express.static('uploads'))

//not valid routes and error handling
app.use((req,res,next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error,req,res,next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;