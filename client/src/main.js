import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';
import Notifications from 'vue-notification';
import rate from 'vue-rate';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './App.vue';
import router from './router';
import store from './store';

// setting up api base url
axios.defaults.baseURL = process.env.VUE_APP_API || 'http://localhost:5000';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Notifications);
Vue.use(rate);

//icons
library.add(fas)
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
