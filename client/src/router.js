import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import BookPage from './views/BookPage.vue';
import UserPage from './views/UserPage.vue';
import UserList from './views/UserList.vue';
import Search from './views/Search';
import SearchResults from './views/SearchResults';
import ComparePage from './views/ComparePage';
import UserRequestBook from './views/UserRequestBook';
import UserRequestsShow from './views/UserRequestsShow';
import AdminBookRequests from './views/admin/ReviewBookRequests';
import AdminManageUser from './views/admin/ManageUsers';


import NewBook from './views/admin/NewBook';

import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/compare-user/:id',
      component: ComparePage
    },
    {
      path: '/book/:id',
      component: BookPage,
    },
    {
      path: '/user/:id',
      component: UserPage,
    },
    {
      path: '/list/:id',
      component: UserList,
      beforeEnter(to, from, next) {
        if (!store.getters.userIsLogged) {
          next("/login");
        } else {
          next();
        }
      }
    },
    {
      path: '/search',
      component: Search
    },
    {
      name: "SearchResults",
      path: '/search/results',
      component: SearchResults,
      props: true
    },
    {
      path: '/requestbook/new',
      component: UserRequestBook,
      beforeEnter(to, from, next) {
        if (!store.getters.userIsLogged) {
            next("/login");
        } else {
            next();
        }
      }
    },
    {
      path: '/requestbook/view',
      component: UserRequestsShow,
      beforeEnter(to, from, next) {
        if (!store.getters.userIsLogged) {
            next("/login");
        } else {
            next();
        }
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter(to, from, next) {
        if (store.getters.userIsLogged) {
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      beforeEnter(to, from, next) {
        if (store.getters.userIsLogged) {
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: '/admin/newbook',
      name: 'newBook',
      component: NewBook,
      beforeEnter(to, from, next) {
        if (store.getters.userIsAdmin) {
          next();
        } else {
          next("/");
        }
      }
    },
    {
      path: '/admin/bookrequests',
      component: AdminBookRequests,
      beforeEnter(to, from, next) {
          if (store.getters.userIsAdmin) {
              next();
          } else {
              next("/");
          }
      }
    },
    {
      path: '/admin/manageusers',
      component: AdminManageUser,
      beforeEnter(to, from, next) {
        if (store.getters.userIsAdmin) {
          next();
        } else {
          next("/");
        }
      }
    }
  ],
});
