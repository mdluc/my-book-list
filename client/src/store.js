import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from './router'
import jwt_decode from 'jwt-decode';
import createPersistedState from 'vuex-persistedstate';

import {showNotification} from "./utilities";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        idToken: localStorage.getItem('token'),
        idUser: null,
        username: null,
        role: null,
    },
    getters: {
        userIsLogged: state => {
            return (state.idToken !== null && state.idUser !== null && state.username !== null);
        },

        userIsAdmin: state => {
            return state.role === 'admin';
        },

        userToken: state => {
            return state.idToken;
        }
    },
    mutations: {
        authUser(state, payload) {
            state.idToken = payload.token;
            state.idUser = payload.userData.userId;
            state.username = payload.userData.username;
            state.role = payload.userData.role;
        },
        clearUserData(state) {
            localStorage.removeItem('token');
            state.idToken = null;
            state.idUser = null;
            state.username = null;
            state.role = null;
        }
    },
    actions: {
        login({commit}, authData) {
            axios.post('/users/signin', {
                email: authData.email,
                password: authData.password,
            })
                .then((response) => {
                    const userData = jwt_decode(response.data.token);
                    localStorage.setItem('token', response.data.token);
                    commit('authUser', {
                        userData: userData,
                        token: response.data.token
                    });

                    router.push("/");
                    showNotification({
                        type: "success",
                        title: "Login successfully",
                        text: `Welcome ${userData.username}`
                    });
                })
                .catch((error) => {
                    showNotification({
                        type: "error",
                        title: error.response.statusText,
                        text: error.response.data.message
                    });
                });
        },
        register({commit}, authData) {
            axios.post('/users/signup', {
                username: authData.username,
                email: authData.email,
                password: authData.password,
            })
            .then((response) => {
                router.push('/login')
                showNotification({
                    type: "success",
                    title: "User Created",
                    text: "Please log in with your credentials!"
                });
            })
            .catch((error) => {
                showNotification({
                    type: "error",
                    title: error.response.statusText,
                    text: error.response.data.message
                });
            })
        },
        logout({commit}) {
            commit('clearUserData');
            router.push("/login");

            showNotification({
                type: "success",
                title: "Logged out successfully",
                text: "See you anytime soon!"
            });
        },

        adminAddBook({commit, state}, bookData) {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + state.idToken
                }
            };

            axios.post('/books/new', bookData, config)
                .then((res) => {
                    showNotification({
                        type: "success",
                        title: "Book added",
                        text: "You added a book!"
                    });
                    router.push(`/book/${res.data.newBookid}`);
                })
                .catch((error) => {
                    showNotification({
                        type: "error",
                        title: "Error",
                        text: error.response.data.message
                    });
                });
        },

        userRequestBook({commit, state}, bookData) {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + state.idToken
                }
            };

            axios.post('/books/request/new', bookData, config)
                .then((res) => {
                    showNotification({
                        type: "success",
                        title: "Request added",
                        text: "You requested a book!"
                    });
                    router.push('/');
                })
                .catch((error) => {
                    showNotification({
                        type: "error",
                        title: "Error",
                        text: error.response.data.message
                    });
                });
        },

        userAddReview({commit, state}, reviewData) {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + state.idToken
                }
            };

            if(reviewData.content !== null && reviewData.rating !== null) {
                axios.post('reviews/add', {
                    reviewContent: reviewData.content,
                    reviewRating: reviewData.rating,
                    bookId: reviewData.bookId
                }, config)
                    .then((response) => {
                        showNotification({
                            type: "success",
                            title: "Review added",
                            text: "You added a review!"
                        });

                        window.setTimeout(function(){
                            window.location.reload()
                        }, 1000);
                    })
                    .catch((error) => {
                        showNotification({
                            type: "error",
                            title: error.response.statusText,
                            text: error.response.data.message
                        });
                    });
            } else {
                showNotification({
                    type: "error",
                    title: "Some fields are empty",
                    text: "Please type something and give a rating!"
                });
            }
        },

        changeUserPrivacy({commit, state}, userData) {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + state.idToken
                }
            };

            axios.post(`/users/changeProfilePrivacy/${userData.id}`, userData,config)
                .then(info => {
                    showNotification({
                        type: "success",
                        title: "Privacy change",
                        text: info.data.message
                    });
                })
                .catch(error => {
                    showNotification({
                        type: "error",
                        title: "Error",
                        text: error.data.message
                    });
                })
        }
    },
    plugins: [createPersistedState()],
});

