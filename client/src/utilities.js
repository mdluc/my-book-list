import Vue from 'vue';

export function showNotification(notificationBody) {
    Vue.notify({
        group: "app-notifications",
        type: notificationBody.type,
        title: notificationBody.title,
        text: notificationBody.text
    });
}