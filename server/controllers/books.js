const Book = require('../models').Book;
const Review = require('../models').Review;
const BookList = require('../models').BookList;
const Request = require('../models').RequestedBooks;
const sequelize = require('sequelize');
const Op = sequelize.Op;
const utils = require('../utils');

const jwt = require('jsonwebtoken');
const datejs = require('datejs');
const ISBN = require('isbn-validate');

module.exports = {
    newBook(req, res, next) {

        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, async function (err, decoded) {
            if (!decoded) {
                utils.deleteImage(req.file.filename);
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {

                let isDuplicate = await utils.checkBookDuplicate(req.body.title, req.body.author, req.body.isbn);

                if (isDuplicate) {
                    utils.deleteImage(req.file.filename);
                    return res.status(409).json({
                        message: "This book already exists!"
                    })
                } else {
                    const date = new Date.parse(req.body.date).toString("MMMM yyyy");

                    // ISBN patterns

                    if (!ISBN.Validate(req.body.isbn)) {
                        return res.status(400).json({
                            message: "ISBN is nod valid!"
                        })
                    }

                    Book.create({
                        title: req.body.title,
                        author: req.body.author,
                        releaseDate: date,
                        isbn: req.body.isbn,
                        imagePath: req.file.filename,
                        description: req.body.description,
                        genres: `{${req.body.genres}}`,
                        publisher: req.body.publisher
                    }).then(result => {
                        res.status(201).json({
                            message: 'Book added!',
                            newBookid: result.id
                        })
                    }).catch(err => {
                        console.log(err)
                        res.status(500).json({
                            error: err
                        });
                    });
                }
            }
        });
    },

    getInfoBook(req, res, next) {
        Book.findById(req.params.bookID)
            .then(bookInfo => {
            if(bookInfo === null) {
                return res.status(404).json({
                    message: "Book not found"
                });
            } else {
                const formatedRelease = bookInfo.releaseDate.format("M Y");
                const formatedGenres = bookInfo.genres.join(", ");
                var avgRating = null;

                const query = `SELECT avg(rating) FROM "Reviews" WHERE "bookId" = ${req.params.bookID};`;

                Review.sequelize.query(query).then(([results]) => {
                    if (results[0]["avg"] === null) {
                        avgRating = "0.00"
                    } else {
                        avgRating = results[0]["avg"].substring(0, 4);
                    }

                    const token = req.headers['authorization'].substring(7);

                    jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {

                        var bookStatusUser = null;
                        var reviewdByUser = false;

                        if(decoded) {
                            BookList.findOne({
                                where: {
                                    idUser: decoded.userId,
                                    idBook: req.params.bookID
                                }
                            }).then(userList => {
                                if(userList !== null && userList.status !== null) {
                                    bookStatusUser = userList.status
                                }

                                Review.count({
                                    where: {
                                        idUser: decoded.userId,
                                        bookId: req.params.bookID
                                    }
                                }).then(reviews => {
                                    if(reviews > 0) {
                                        reviewdByUser = true
                                    }

                                    return res.status(200).json({
                                        title: bookInfo.title,
                                        author: bookInfo.author,
                                        releaseDate: formatedRelease,
                                        isbn: bookInfo.isbn,
                                        imagePath: bookInfo.imagePath,
                                        description: bookInfo.description.replace(new RegExp('\r?\n','g'), '<br />'),
                                        genres: formatedGenres,
                                        publisher: bookInfo.publisher,
                                        avgRating: avgRating,
                                        bookStatusUser: bookStatusUser,
                                        reviewdByUser: reviewdByUser
                                    });
                                })
                            })
                        } else {
                            return res.status(200).json({
                                title: bookInfo.title,
                                author: bookInfo.author,
                                releaseDate: formatedRelease,
                                isbn: bookInfo.isbn,
                                imagePath: bookInfo.imagePath,
                                description: bookInfo.description.replace(new RegExp('\r?\n','g'), '<br />'),
                                genres: formatedGenres,
                                publisher: bookInfo.publisher,
                                avgRating: avgRating,
                                bookStatusUser: bookStatusUser
                            });
                        }
                    });
                });
            }
        })
    },
    getHomePageBooks(req, res, next) {
        Book.findAll({
            order: [ [ sequelize.fn('RANDOM') ] ],
            limit: 6
        }).then(books => {
            return res.status(200).json({
                books: books
            });
        }).catch(error => {
            console.log(error)
            return res.status(500).json({
                error: error
            });
        })
    },
    search(req, res, next) {
        var whereCondition = {
            where: {
                title: {
                    [Op.iLike]: `%${req.body.title}%`
                },
                author: {
                    [Op.iLike]: `%${req.body.author}%`
                },
                publisher: {
                    [Op.iLike]: `%${req.body.publisher}%`
                },
                isbn: {
                    [Op.iLike]: `%${req.body.isbn}%`
                },
                genres: {
                    [Op.contains]: req.body.genres
                }
            }
        };

        if (req.body.title === undefined || req.body.title === null) {
            delete whereCondition.where.title
        }

        if (req.body.author === undefined || req.body.author === null) {
            delete whereCondition.where.author
        }

        if (req.body.publisher === undefined || req.body.publisher === null) {
            delete whereCondition.where.publisher
        }

        if (req.body.isbn === undefined || req.body.isbn === null) {
            delete whereCondition.where.isbn
        }

        if (req.body.genres === undefined || req.body.genres === null || req.body.genres.includes(null)) {
            delete whereCondition.where.genres
        }

        Book.findAll(whereCondition).then(results => {
            return res.status(200).json({
                results: results
            })
        }).catch(error => {
            return res.status(500).json({
                error: error
            });
        })
    },

    getRating(req, res, next) {
        var avgRating = null;
        const query = `SELECT avg(rating) FROM "Reviews" WHERE "bookId" = ${req.params.bookID};`;

        Review.sequelize.query(query).then(([results]) => {
            if (results[0]["avg"] === null) {
                avgRating = "0.00"
            } else {
                avgRating = results[0]["avg"].substring(0, 4);
            }

            return res.status(200).json({
                rating: avgRating
            })

        }).catch(error => {
            return res.status(500).json({
                error: error
            });
        })
    },

    newRequest(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, async function (err, decoded) {
            if (!decoded) {
                utils.deleteImage(req.file.filename);
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {

                let bookDuplicate = await utils.checkBookDuplicate(req.body.title, req.body.author, req.body.isbn);
                let requestDuplicate = await utils.checkRequestDuplicate(req.body.title, req.body.author, req.body.isbn);
                let isDuplicate = bookDuplicate && requestDuplicate;

                if (isDuplicate) {
                    utils.deleteImage(req.file.filename);
                    return res.status(409).json({
                        message: "This book already exists!"
                    })
                } else {
                    const date = new Date.parse(req.body.date).toString("MMMM yyyy");

                    // ISBN patterns

                    if (!ISBN.Validate(req.body.isbn)) {
                        return res.status(400).json({
                            message: "ISBN is nod valid!"
                        })
                    }

                    Request.create({
                        title: req.body.title,
                        author: req.body.author,
                        releaseDate: date,
                        isbn: req.body.isbn,
                        imagePath: req.file.filename,
                        description: req.body.description,
                        genres: `{${req.body.genres}}`,
                        publisher: req.body.publisher,
                        status: 'pending',
                        userID: decoded.userId
                    }).then(result => {
                        res.status(201).json({
                            message: 'Request added!',
                            newRequestid: result.id
                        })
                    }).catch(err => {
                        console.log(err)
                        res.status(500).json({
                            error: err
                        });
                    });
                }
            }
        });
    }
};