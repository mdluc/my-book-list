const BookList = require('../models').BookList;
const User = require('../models').User;
const Review = require('../models').Review;
const Comments = require('../models').Comments;
const Book = require('../models').Book;
const jwt = require('jsonwebtoken');
const datejs = require('datejs');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;
const utils = require('../utils');

module.exports = {
    getInfo(req, res, next) {

        User.findById(req.params.id)
            .then(user => {

                //number of days
                let date = new Date();
                let joinedDate = user.createdAt;

                //milliseconds in a day = 8.64e7
                let numberOfDays = Math.round(Math.abs((+date) - (+joinedDate)) / 8.64e7);


                // number of reviews
                let numberOfReviews, numberOfComments;
                Review.count({where: {'idUser': {[Op.eq]: req.params.id}} })
                    .then(c => {
                        numberOfReviews = c;

                        Comments.count({
                            where: {
                                'authorID': {
                                    [Op.eq]: req.params.id
                                }
                            }
                        }).then(c2 => {
                            numberOfComments = c2

                            //mean score
                            const scoreQuery = `SELECT avg(rating) FROM "Reviews" WHERE "idUser" = ${req.params.id};`;
                            Review.sequelize.query(scoreQuery).then(([meanScoreRes]) => {
                                let meanScore;

                                if (meanScoreRes[0]["avg"] === null) {
                                    meanScore = "0.00"
                                } else {
                                    meanScore = meanScoreRes[0]["avg"].substring(0, 4);
                                }

                                const query = `select count(case status when 'reading' then 1 else null end) as "readingBooks", count(case status when 'finished' then 1 else null end) as "finishedBooks", count(case status when 'planned' then 1 else null end) as "plannedBooks", count(case status when 'unfinished' then 1 else null end) as "unfinishedBooks" from "BookLists" where "idUser" = ${req.params.id}`;

                                BookList.sequelize.query(query).then((booksCounts) => {
                                    console.log(booksCounts[0][0])
                                    return res.status(201).json({
                                        numberOfDays: numberOfDays,
                                        numberOfReviews: numberOfReviews,
                                        numberOfComments: numberOfComments,
                                        meanScore: meanScore,
                                        reading: booksCounts[0][0].readingBooks,
                                        finished: booksCounts[0][0].finishedBooks,
                                        planned: booksCounts[0][0].plannedBooks,
                                        unfinished: booksCounts[0][0].unfinishedBooks,
                                        username: user.username,
                                        privateProfile: user.privateProfile
                                    })
                                }).catch(error => {
                                    return res.status(500).json({
                                        error: error
                                    })
                                })
                            })
                        })
                    });
            })
            .catch(error => {
                return res.status(500).json({
                    message: error,
                })
            })
    },

    getList(req, res, next) {

        BookList.findAll({
            where: {
                idUser: req.params.id,
                status: "reading"
            },
            order: [
                [Book, 'title', 'ASC'],
            ],
            include: [{
                model: Book,
            }]
        }).then(reading => {

            BookList.findAll({
                where: {
                    idUser: req.params.id,
                    status: "finished"
                },
                order: [
                    [Book, 'title', 'ASC'],
                ],
                include: [{
                    model: Book,
                }]
            }).then(finished => {

                BookList.findAll({
                    where: {
                        idUser: req.params.id,
                        status: "planned"
                    },
                    order: [
                        [Book, 'title', 'ASC'],
                    ],
                    include: [{
                        model: Book,
                    }]
                }).then(planned => {

                    BookList.findAll({
                        where: {
                            idUser: req.params.id,
                            status: "unfinished"
                        },
                        order: [
                            [Book, 'title', 'ASC'],
                        ],
                        include: [{
                            model: Book,
                        }]
                    }).then(unfinished => {
                        User.findById(req.params.id)
                            .then(user => {
                                return res.status(201).json({
                                    reading: reading,
                                    finished: finished,
                                    planned: planned,
                                    unfinished: unfinished,
                                    username: user.username,
                                    privateProfile: user.privateProfile
                                })
                            })
                    })
                })
            })
        })
    },

    updateUserBook(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
            if(!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                BookList.findOrCreate({
                    where: {
                        idUser: decoded.userId,
                        idBook: req.params.id
                    },
                    defaults: {
                        status: req.body.status
                    }
                }).then(([bookStatus, created]) => {
                    if(created) {
                        return res.status(201).json({
                            message: "Book added to your list!"
                        })
                    } else {
                        bookStatus.update({
                            status: req.body.status
                        }).then(updated => {
                            return res.status(201).json({
                                message: "Status changed!"
                            })
                        }).catch(error => {
                            return res.status(500).json({
                                error: error
                            })
                        })
                    }
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    deleteUserBook(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
            if(!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                BookList.destroy({
                    where: {
                        idUser: decoded.userId,
                        idBook: req.params.id
                    }
                }).then(result => {

                    if(result) {
                        return res.status(201).json({
                            message: "Book removed from your list!"
                        })
                    } else {
                        return res.status(404).json({
                            message: "Book not found in list!"
                        })
                    }

                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    getUserActivity(req, res, next) {
        const query = `SELECT BookList.status, BookList."idBook", Book.title, Book.author, Review.rating FROM "BookLists" BookList FULL JOIN "Books" Book on BookList."idBook" = Book.id FULL JOIN "Reviews" Review on Book.id = Review."bookId" and Review."idUser" = ${req.params.id} WHERE BookList."idUser" = ${req.params.id} order by BookList."updatedAt" DESC LIMIT 5`;

        BookList.sequelize.query(query).then(([results]) => {
            return res.status(200).json({
                activity: results,
            })
        }).catch(error => {
            return res.status(500).json({
                error: error,
            })
        })
    },

    getUserBookStatus(req,res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
            if(decoded) {
                BookList.find({
                    where: {
                        idUser: decoded.userId,
                        idBook: req.params.book
                    }
                }).then(result => {
                    let bookStatus = result !== null ? result.status : null;
                    return res.status(200).json({
                        status: bookStatus,
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error,
                    })
                })
            } else {
                return res.status(401).json({
                    error: "Unauthorized",
                })
            }
        })
    },

    getUserCompatibility(req, res, next) {

        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, async function (err, decoded) {
            if (decoded) {
                var compatibility = await utils.getCompatibility(decoded.userId, req.params.id);

                return res.status(200).json({
                    compatibility: compatibility
                })
            } else {
                return res.status(401).json({
                    error: "Unauthorized",
                })
            }
        })
    }
};