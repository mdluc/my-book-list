const Review = require('../models').Review;
const jwt = require('jsonwebtoken');
const datejs = require('datejs');

module.exports = {
    addReview(req, res, next) {

        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
            if(!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {

                console.log(req.body.bookId)
                Review.create({
                    idUser: decoded.userId,
                    bookId: req.body.bookId,
                    content: req.body.reviewContent,
                    rating: req.body.reviewRating
                }).then(result => {
                    return res.status(201).json({
                        message: "Review added!",
                    });
                }).catch(err => {
                    console.log(err);
                    return res.status(500).json({
                        error: err
                    });
                });
            }
        });
    },

    getBookReviews(req, res, next) {

        const query = `SELECT "Users".id, "Users".username, "Reviews".content, "Reviews".rating, "Reviews"."createdAt" from "Users" join "Reviews" on "Users".id = "Reviews"."idUser" and "Reviews"."bookId" = ${req.params.id} order by "Reviews"."createdAt" DESC`

        Review.sequelize.query(query).then(([results]) => {

            for (i=0; i<results.length; i++) {
                results[i]["createdAt"] = results[i]["createdAt"].toString("dd MMMM yyyy HH:mm")
            }

            return res.status(200).json({
                reviews: results
            });
        })
    }
};