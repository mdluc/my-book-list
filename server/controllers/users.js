const User = require('../models').User;
const Comments = require('../models').Comments;
const Book = require('../models').Book;
const Request = require('../models/').RequestedBooks;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const utils = require('../utils');
const datejs = require('datejs');
const Op = Sequelize.Op;

module.exports = {
    signUp(req, res, next) {
        User.findAll({
            where: {
                email: req.body.email
            }
        }).then(users => {
            if (users.length >= 1) {
                return res.status(409).json({
                    message: 'Email already exists!'
                });
            } else {
                User.findAll({
                    where: {
                        username: req.body.username
                    }
                }).then(usernames => {
                    if (usernames.length >= 1) {
                        return res.status(409).json({
                            message: 'Username already exists!'
                        });
                    } else {
                        bcrypt.hash(req.body.password, 10, (err, hash) => {
                            if (err) {
                                return res.status(500).json({
                                    error: err
                                });
                            } else {
                                User.create({
                                    username: req.body.username,
                                    password: hash,
                                    email: req.body.email
                                }).then(result => {
                                    res.status(201).json({
                                        message: 'User created!'
                                    })
                                }).catch(err => {
                                    res.status(500).json({
                                        error: err
                                    });
                                });
                            }
                        });
                    }
                })
            }
        });
    },

    signIn(req, res, next) {
        User.findAll({
            where: {
                email: req.body.email
            }
        }).then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'Check your email or password!'
                });
            }

            bcrypt.compare(req.body.password, user[0].dataValues.password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Check your email or password!'
                    });
                }

                if (result) {
                    const token = jwt.sign({
                            userId: user[0].dataValues.id,
                            username: user[0].dataValues.username,
                            role: user[0].dataValues.role
                        },
                        process.env.JWT_KEY, {
                            expiresIn: '7d'
                        }
                    );

                    return res.status(200).json({
                        message: 'Auth successful!',
                        token: token
                    });
                }

                res.status(401).json({
                    message: 'Check your email or password!'
                });
            })
        })
    },

    changeProfilePrivacy(req, res, next) {

        const token = req.headers['authorization'].substring(7);
        const userProfile = parseInt(req.params.profileID);

        jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
            if(!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                if(userProfile !== decoded.userId) {
                    return res.status(401).json({
                        message: "You don't have permission!"
                    })
                } else {

                    User.update({
                        privateProfile: Sequelize.literal('NOT "privateProfile"'),
                    }, {
                        where: {
                            id: userProfile
                        }
                    }).then(updated => {
                        return res.status(200).json({
                            message: "Profile privacy has been updated"
                        })
                    }).catch(error => {
                        return res.status(500).json({
                            message: error
                        })
                    })
                }
            }
        });
    },

    async compareLists(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        var privateProfile = await utils.checkProfilePrivacy(req.params.userID);

        if (privateProfile) {
            return res.status(401).json({
                message: "This profile is private!"
            })
        } else {
            jwt.verify(token, process.env.JWT_KEY, async function (err, decoded) {
                if (!decoded) {
                    return res.status(401).json({
                        message: "You don't have permission!"
                    })
                } else {
                    let lists = await utils.compareLists(decoded.userId, req.params.userID);

                    let booksLists = await utils.getBooksLists(lists);

                    User.findById(req.params.userID).then(user => {
                        return res.status(200).json({
                            uniqueA: booksLists[0],
                            uniqueB: booksLists[1],
                            matches: booksLists[2],
                            username: user.username
                        })
                    })
                }
            });
        }
    },

    newUserComment(req, res, next) {

        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                Comments.create({
                    authorID: decoded.userId,
                    userID: req.params.id,
                    comment: req.body.comment,
                    parentID: req.body.parentID
                }).then(result => {
                    return res.status(200).json({
                        message: "Comment added"
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    fetchUserComment(req, res, next) {

        const query = `SELECT "Users".id, "Users".username, "Comments".id as "commentID" ,"Comments".comment, "Comments"."createdAt", "Comments"."parentID" from "Users" join "Comments" on "Users".id = "Comments"."authorID" and "Comments"."userID" = ${req.params.id} order by "Comments"."createdAt" DESC`

        Comments.sequelize.query(query).then(([results]) => {

            for (i=0; i<results.length; i++) {
                results[i]["createdAt"] = results[i]["createdAt"].toString("dd MMMM yyyy HH:mm")
            }

            return res.status(200).json({
                comments: results
            });
        }).catch(error => {
            return res.status(500).json({
                message: error
            });
        })
    },

    async deleteComment(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        let isOkToDelete = await utils.checkAuthor(token, req.params.id);

        if (isOkToDelete) {
            Comments.destroy({
                where: {
                    id: req.params.id
                }
            }).then(result => {
                return res.status(200).json({
                    message: "Comment deleted"
                })
            }).catch(error => {
                return res.status(500).json({
                    message: error
                })
            })
        } else {
            return res.status(401).json({
                message: "You don't have permission!"
            })
        }
    },

    getFullThread(req, res, next) {

        const query = `SELECT "Users".id, "Users".username, "Comments".id as "commentID" ,"Comments".comment, "Comments"."createdAt", "Comments"."parentID" from "Users" join "Comments" on "Users".id = "Comments"."authorID" and "Comments"."userID" = ${req.body.userID} where "Comments"."parentID" = ${req.params.parentID} or "Comments"."id" = ${req.params.parentID}  order by "Comments"."createdAt" ASC`;

        Comments.sequelize.query(query).then(([result]) => {

            for (i=0; i<result.length; i++) {
                result[i]["createdAt"] = result[i]["createdAt"].toString("dd MMMM yyyy HH:mm")
            }

            let parent = result.shift()
            return res.status(200).json({
                parent: parent,
                comments: result
            });
        }).catch(error => {
            return res.status(500).json({
                message: error
            })
        })
    },

    getUserRequests(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded) {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                Request.findAll({
                    where: {
                        userID: decoded.userId
                    },
                    attributes: ['title', 'author', 'status'],
                    order: [
                        ['updatedAt', 'DESC']
                    ]
                }).then(result => {
                    return res.status(200).json({
                        requests: result
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    getAdminRequests(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded || decoded.role === "standard") {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                Request.findAll({
                    where: {
                        status: "pending"
                    },
                    attributes: ['id', 'title', 'author'],
                    order: [
                        ['updatedAt', 'DESC']
                    ]
                }).then(pending => {
                    Request.findAll({
                        where: {
                            status: {
                                [Op.not]: 'pending'
                            }
                        },
                        attributes: ['title', 'author', 'status'],
                        order: [
                            ['updatedAt', 'DESC']
                        ]
                    }).then(completed => {
                        return res.status(200).json({
                            activeRequest: pending,
                            reviewedRequests: completed
                        })
                    }).catch(error => {
                        return res.status(500).json({
                            error: error
                        })
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    fetchRequestInfo(req, res, next) {
        Request.findById(req.params.requestID).then(result => {
            result = result.toJSON()
            result.formatedReleaseDate = result.releaseDate.format("M Y");
            result.formatedGenres =  result.genres.join(', ')
            // console.log(result.releaseDate.format("M Y"))
            return res.status(200).json({
                request: result,
            })
        }).catch(error => {
            return res.status(500).json({
                error: error
            })
        })
    },

    changeRequestStatus(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded || decoded.role === "standard") {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                Request.findById(req.params.requestID)
                    .then(request => {
                        if(req.body.status === "rejected") {
                            request.update({
                                status: req.body.status
                            }).then(rejected => {
                                return res.status(200).json({
                                    message: "Book rejected"
                                })
                            })
                        }

                        if(req.body.status === "accepted") {
                            Book.create({
                                title: request.title,
                                author: request.author,
                                releaseDate: request.releaseDate,
                                isbn: request.isbn,
                                imagePath: request.imagePath,
                                description: request.description,
                                genres: `{${request.genres}}`,
                                publisher: request.publisher
                            }).then(newbook => {
                                request.update({
                                    status: req.body.status
                                }).then(accepted => {
                                    return res.status(200).json({
                                        message: "Book approved"
                                    })
                                })
                            }).catch(error => {
                                return res.status(500).json({
                                    error: error
                                })
                            })
                        }
                    })
            }
        });
    },

    getUsersList(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded || decoded.role === "standard") {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                User.findAll({
                    where: {
                        role: {
                            [Op.not]: 'admin'
                        }
                    },
                    attributes: ["id", "username", "role"],
                    order: [
                        ['username', 'ASC']
                    ]
                }).then(list => {
                    return res.status(200).json({
                        listOfUsers: list
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    },

    deleteUser(req, res, next) {
        const token = req.headers['authorization'].substring(7);

        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (!decoded || decoded.role === "standard") {
                return res.status(401).json({
                    message: "You don't have permission!"
                })
            } else {
                User.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then(result => {
                    return res.status(200).json({
                        message: "User deleted"
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error: error
                    })
                })
            }
        });
    }
};