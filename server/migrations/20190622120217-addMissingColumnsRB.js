'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return Promise.all([
        queryInterface.addColumn(
            'RequestedBooks',
            'userID',
            {
                type: Sequelize.INTEGER,
                allowNull: false
            }
        ),
        queryInterface.addColumn(
            'RequestedBooks',
            'status',
            {
                type: Sequelize.STRING,
                allowNull: false
            }
        )
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
