'use strict';
module.exports = (sequelize, DataTypes) => {
  var Book = sequelize.define('Book', {
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    releaseDate: DataTypes.DATE,
    isbn: DataTypes.STRING,
    imagePath: DataTypes.STRING,
    description: DataTypes.TEXT,
    genres: DataTypes.STRING,
    publisher: DataTypes.STRING
  }, {});
  Book.associate = function(models) {
    // associations can be defined here
    Book.belongsTo(models.BookList, {foreignKey: 'id'});
  };
  return Book;
};