'use strict';
module.exports = (sequelize, DataTypes) => {
  var BookList = sequelize.define('BookList', {
    idBook: DataTypes.INTEGER,
    idUser: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {});
  BookList.associate = function(models) {
    // associations can be defined here
    BookList.hasMany(models.Book, {foreignKey: 'id',sourceKey: 'idBook'});
  };
  return BookList;
};