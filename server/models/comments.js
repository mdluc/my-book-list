'use strict';
module.exports = (sequelize, DataTypes) => {
  var Comments = sequelize.define('Comments', {
    authorID: DataTypes.INTEGER,
    userID: DataTypes.INTEGER,
    comment: DataTypes.STRING,
    parentID: DataTypes.INTEGER
  }, {});
  Comments.associate = function(models) {
    // associations can be defined here
  };
  return Comments;
};