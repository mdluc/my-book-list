'use strict';
module.exports = (sequelize, DataTypes) => {
  var RequestedBooks = sequelize.define('RequestedBooks', {
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    releaseDate: DataTypes.DATE,
    isbn: DataTypes.STRING,
    imagePath: DataTypes.STRING,
    description: DataTypes.TEXT,
    genres: DataTypes.STRING,
    publisher: DataTypes.STRING,
    userID: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {});
  RequestedBooks.associate = function(models) {
    // associations can be defined here
  };
  return RequestedBooks;
};