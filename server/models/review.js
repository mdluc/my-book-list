'use strict';
module.exports = (sequelize, DataTypes) => {
  var Review = sequelize.define('Review', {
    idUser: DataTypes.INTEGER,
    content: DataTypes.STRING,
    rating: DataTypes.DECIMAL(2,2),
    bookId: DataTypes.INTEGER
  }, {});
  Review.associate = function(models) {
    // associations can be defined here
  };
  return Review;
};