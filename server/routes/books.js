const express = require('express');
const router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        fileExtension = file.originalname.split('.')[1];
        cb(null, Date.now() + '.' + fileExtension)
    }
});

const upload = multer({storage: storage});


const BooksController = require('../controllers/books');

router.post('/new', upload.single('bookImage'),BooksController.newBook);
router.get('/info/:bookID', BooksController.getInfoBook);
router.get('/featured', BooksController.getHomePageBooks);
router.get('/getrating/:bookID', BooksController.getRating);
router.post('/search', BooksController.search);
router.post('/request/new',upload.single('bookImage') , BooksController.newRequest);

module.exports = router;