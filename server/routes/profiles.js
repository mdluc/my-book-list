const express = require('express');
const router = express.Router();

const ProfilesController = require('../controllers/profiles');

router.get('/info/:id', ProfilesController.getInfo);
router.get('/list/:id', ProfilesController.getList);
router.get('/list/status/:book', ProfilesController.getUserBookStatus);
router.post('/list/updatestatus/:id', ProfilesController.updateUserBook);
router.delete('/list/deletestatus/:id', ProfilesController.deleteUserBook);
router.get('/list/activity/:id', ProfilesController.getUserActivity);
router.get('/list/compatibility/:id', ProfilesController.getUserCompatibility);

module.exports = router;