const express = require('express');
const router = express.Router();

const ReviewsController = require('../controllers/reviews');

router.post('/add', ReviewsController.addReview);
router.get('/book/:id', ReviewsController.getBookReviews);

module.exports = router;