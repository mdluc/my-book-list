const express = require('express');
const router = express.Router();

const UsersController = require('../controllers/users');

router.post('/signup', UsersController.signUp);
router.post('/signin', UsersController.signIn);
router.post('/changeProfilePrivacy/:profileID', UsersController.changeProfilePrivacy);
router.get('/compare/:userID', UsersController.compareLists);
router.post('/new/comment/:id', UsersController.newUserComment);
router.get('/get/comments/:id', UsersController.fetchUserComment);
router.delete('/delete/comment/:id', UsersController.deleteComment);
router.post("/get/comments/thread/:parentID", UsersController.getFullThread);
router.get('/get/requests', UsersController.getUserRequests);
router.get('/get/adminrequests', UsersController.getAdminRequests);
router.get('/get/requestInfo/:requestID', UsersController.fetchRequestInfo);
router.post('/change/requestStatus/:requestID', UsersController.changeRequestStatus);
router.get('/get/userslist', UsersController.getUsersList);
router.delete('/delete/user/:id', UsersController.deleteUser);

module.exports = router;