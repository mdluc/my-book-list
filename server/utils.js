const BookList = require('./models/').BookList;
const Request = require('./models/').RequestedBooks;
const Book = require('./models/').Book;
const User = require('./models/').User;
const Comments = require('./models').Comments;
const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = {
    compareLists: function (loggedUser, comparedUser) {
        return new Promise(function (fulfill, reject) {
            let userAList, userBList, uniqueA, uniqueB, matches;
            BookList.findAll({
                where: {
                    idUser: loggedUser
                },
                attributes: ['idBook']
            }).then(books => {
                userAList = books.map(book => book.idBook)
                console.log(userAList)

                BookList.findAll({
                    where: {
                        idUser: comparedUser
                    },
                    attributes: ['idBook']
                }).then(books => {
                    userBList = books.map(book => book.idBook)
                    console.log(userBList)

                    uniqueA = userAList.filter(function (id) {
                        return !userBList.includes(id)
                    });

                    uniqueB = userBList.filter(function (id) {
                        return !userAList.includes(id)
                    });

                    matches = userAList.filter(function (id) {
                        return userBList.includes(id)
                    });

                    fulfill([uniqueA, uniqueB, matches]);
                })
            })
        })
    },
    getBooksLists:function (lists) {
        return new Promise(function (fulfill, reject) {
            let uniqueA, uniqueB, matches;

           Book.findAll({
               where: {
                   id: lists[0]
               },
               attributes: ['id', 'title', "author"],
               order: [
                   ['title']
               ]
           }).then(result => {
               uniqueA = result;

               Book.findAll({
                   where: {
                       id: lists[1]
                   },
                   attributes: ['id', 'title', "author"],
                   order: [
                       ['title']
                   ]
               }).then(result2 => {
                   uniqueB = result2;

                   Book.findAll({
                       where: {
                           id: lists[2]
                       },
                       attributes: ['id', 'title', "author"],
                       order: [
                           ['title']
                       ]
                   }).then(result3 => {
                       matches = result3;
                       fulfill([uniqueA,uniqueB, matches])
                   })
               })
           })
        })
    },

    checkProfilePrivacy: function (userID) {
        return new Promise(function (fulfill, reject) {
            User.findById(userID).then(result => {
                fulfill(result.privateProfile)
            })
        })
    },
    
    getCompatibility: async function (userID, targetID) {
        let bookLists = await  this.compareLists(userID, targetID);
        let mergedBooks  = bookLists[0].concat(bookLists[1]).concat(bookLists[2]);

        let matchedBooks = bookLists[2].length;
        let allBooks = mergedBooks.length;
        return ((matchedBooks / allBooks) * 100).toFixed(2);
    },

    checkBookDuplicate: function (title, author, isbn) {
        return new Promise(function (fulfill, reject) {
            Book.findAll({
                where: {
                    title: title,
                    author: author,
                    isbn: isbn
                }
            }).then(result => {
                fulfill(result.length >= 1)
            })
        })
    },

    checkRequestDuplicate: function (title, author, isbn) {
        return new Promise(function (fulfill, reject) {
            Request.findAll({
                where: {
                    title: title,
                    author: author,
                    isbn: isbn
                }
            }).then(result => {
                fulfill(result.length >= 1)
            })
        })
    },

    deleteImage: function (imageName) {
        let filepath = `./uploads/${imageName}`;
        fs.unlinkSync(filepath);
    },

    checkAuthor(token, commentID) {
        return new Promise(function (fulfill, reject) {
            jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
                if(!decoded) {
                    fulfill(false)
                } else {
                    Comments.findById(commentID).then(result => {
                        console.log(decoded)
                        if(decoded.role === "admin" || result.authorID === decoded.userId || result.userID === decoded.userId) {
                            fulfill(true)
                        } else {
                            fulfill(false)
                        }
                    })
                }
            });
        })
    }
};